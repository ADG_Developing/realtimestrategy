package Mirrorz.ui;

import java.awt.Font;
import java.awt.Rectangle;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import AE2D.ui.UIFont;
import AE2D.utils.Color;
import AE2D.utils.MouseObject;
import AE2D.utils.MouseUtils;
import AE2D.utils.RenderingManager;
import Mirrorz.Main;
import Mirrorz.game.engine.mechanics.CommandHandler;

public class UITextfield extends MouseObject
{
	private String input = "";
	private String defaultText = "";
	
	private int currentTextInSlot = 0;
	
	private boolean textfieldActivated = false;
	private boolean commandFailed = false;
	private boolean prevState;
	
	private UI uiClass = null;
	private UIFont font = new UIFont();
	private CommandHandler commandHandler = null;
	
	private Rectangle bounds = new Rectangle();
	
	public UITextfield(String defaultText, int x, int y, int boundX, int boundY, UI uiClass, CommandHandler cHandler)
	{
		super(new Rectangle(x, y, boundX, boundY));
		
		bounds.x = x;
		bounds.y = y;
		bounds.width = boundX;
		bounds.height = boundY;
		
		currentTextInSlot = 0;
		this.defaultText = defaultText;
		this.uiClass = uiClass;
		commandHandler = cHandler;
		
		font.initFont("Arial", Font.PLAIN, 15, true);
	}
	
	public void update()
	{
		if(defaultText != "" && defaultText != null)
		{
			if(currentTextInSlot != 2)
			{
				input = defaultText;
				currentTextInSlot = 1;
			}
		}
		
		if(!textfieldActivated && currentTextInSlot != 1)
		{
			input = defaultText;
			currentTextInSlot = 1;
		}
		
		if(bounds.contains(Mouse.getX(), (Main.instance.getHeight() - Mouse.getY())))
		{
			if(Mouse.isButtonDown(0) && !prevState && !MouseUtils.mouseHasObjectGrabbed(this))
			{
				textfieldActivated = !textfieldActivated;
				
				input = "";
				currentTextInSlot = 2;
		        
		        MouseUtils.setGrabbedObject(this);
		    }
			else
			{
				MouseUtils.setUngrabbed();
			}
			
			prevState = Mouse.isButtonDown(0);
		}
		else
		{
			if(textfieldActivated)
			{
				if(Mouse.isButtonDown(0) || Mouse.isButtonDown(1) && !prevState && !MouseUtils.mouseHasObjectGrabbed(this))
				{
					textfieldActivated = !textfieldActivated;
					
					input = "";
					currentTextInSlot = 2;
			        
			        MouseUtils.setGrabbedObject(this);
			    }
				else
				{
					MouseUtils.setUngrabbed();
				}
				
				if(Mouse.isButtonDown(0))
				{
					prevState = Mouse.isButtonDown(0);
				}
				else
				{
					prevState = Mouse.isButtonDown(1);
				}
			}
		}
		
		while (Keyboard.next()) {
			if(Keyboard.getEventKeyState())
			{
				if(textfieldActivated)
				{
					if(!(input.length() > 122))
					{
						if(Keyboard.getEventKey() == Keyboard.KEY_BACK)
						{
							if(input.length() - 1 >= 0)
							{
								input = input.substring(0, input.length() - 1);
								currentTextInSlot = 2;
							}
						}
						else if(Keyboard.getEventKey() == Keyboard.KEY_DELETE)
						{
							input = defaultText;
							currentTextInSlot = 1;
						}
						else if(Keyboard.getEventKey() == Keyboard.KEY_RETURN)
						{
							if(input.startsWith("/"))
							{
								commandFailed = !commandHandler.processInput(input);
							}
							
							input = "";
							currentTextInSlot = 2;
						}
						else if(Keyboard.getEventKey() == Keyboard.KEY_LSHIFT || Keyboard.getEventKey() == Keyboard.KEY_RSHIFT)
						{}
						else if(Keyboard.getEventKey() != Keyboard.KEY_CAPITAL)
						{
							if(currentTextInSlot == 1)
							{
								input = "";
							}
							
							input = input + String.valueOf(Keyboard.getEventCharacter());
							currentTextInSlot = 2;
						}
					}
				}
			}
		}
	}
	
	int counter = 0;
	
	public void render()
	{
		int textPosY = 0 + (bounds.height - font.ttf.getHeight(input)) / 2;
		
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		RenderingManager.drawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, new Color(255, 255, 255, 200, true), true);
		GL11.glDisable(GL11.GL_BLEND);
		
		if(currentTextInSlot == 1)
		{
			font.renderUpdateText(10, textPosY, input, new Color("#888888"));
		}
		else if(currentTextInSlot == 2)
		{
			font.renderUpdateText(10, textPosY, input, new Color("#4D4D4D"));
		}
		
		if(commandFailed)
		{
			counter++;
			
			if(counter >= 500)
			{
				commandFailed = false;
			}
			
			font.renderUpdateText(10, textPosY + 30, "That command has not been found.", new Color(255, 255, 255, true));
		}
	}
}
