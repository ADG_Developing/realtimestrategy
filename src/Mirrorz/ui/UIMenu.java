package Mirrorz.ui;

import java.awt.Font;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import AE2D.ui.UIFont;
import AE2D.utils.Color;
import AE2D.utils.MathUtils;
import AE2D.utils.RenderingManager;

public class UIMenu 
{
	// Class Variables
	private UI uiClass = null;
	private UIFont font = new UIFont();
	public Rectangle bounds = new Rectangle();
	private List<Texture> textures;
	
	// Definition
	public UIMenu(int x, int y, int boundX, int boundY, UI uiClass)
	{
		textures = new ArrayList<Texture>();
		
		bounds.x = x;
		bounds.y = y;
		bounds.width = boundX;
		bounds.height = boundY;
		
		this.uiClass = uiClass;
		font.initFont("Arial", Font.BOLD, 20, true);
	}
	
	// Actions
	public void init()
	{
		try {
			textures.add(0, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "paperroll.png")));
			textures.add(1, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "image_border.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void render(String name, Texture texture)
	{
		int centerTextPosX = bounds.x + (bounds.width - font.ttf.getWidth(name)) / 2;
		
		//RenderingManager.drawRoundedBox(bounds.x, bounds.y, bounds.width, bounds.height, new Color(255, 255, 255, true).toVec4(), new Color(255, 255, 255, true).toVec4(), false);
		RenderingManager.drawTexturedRectangle(bounds.x, bounds.y, bounds.width, bounds.height, textures.get(0));
		font.renderUpdateTextBlend(centerTextPosX, bounds.y + 10, name, new Color(0, 0, 0, false));
		RenderingManager.drawTexturedRectangle(MathUtils.calculateCenterX(bounds.width, 160, bounds.x) - 325, MathUtils.calculateCenterX(bounds.height, 160, bounds.y) - 150, 160, 160, textures.get(1));
		RenderingManager.drawTexturedRectangle(MathUtils.calculateCenterX(bounds.width, 125, bounds.x) - 325, MathUtils.calculateCenterX(bounds.height, 125, bounds.y) - 150, 125, 125, texture);
	}
	
	public void update()
	{
		
	}
}
