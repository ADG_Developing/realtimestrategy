package Mirrorz.ui;

import java.util.ArrayList;
import java.util.List;

import Mirrorz.Main;
import Mirrorz.game.GameEngine;

public class UI 
{
	private List<UIButton> buttons;
	public UITextfield textField;
	
	public void initUI()
	{
		buttons = new ArrayList<UIButton>();
		
		addUIButtons();
		
		textField = new UITextfield("Enter text here.", 0, 0, Main.instance.getWidth(), 30, this, GameEngine.instance.getCommandHandler());
	}
	
	public void addUIButtons()
	{
		buttons.add(0, new UIButton("Button1", 0, Main.instance.getHeight() - 50, 200, 50, this));
	}
	
	public void render()
	{	
		for(UIButton tmp : buttons)
		{
			tmp.render();
		}
		
		textField.render();
	}
	
	public void update(float mouseX, float mouseY)
	{
		textField.update();
		
		for(UIButton tmp : buttons)
		{
			tmp.update(mouseX, mouseY);
		}
	}
}
