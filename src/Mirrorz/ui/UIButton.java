package Mirrorz.ui;

import java.awt.Font;
import java.awt.Rectangle;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector4f;

import AE2D.ui.UIFont;
import AE2D.utils.Color;
import AE2D.utils.MouseObject;
import AE2D.utils.MouseUtils;
import AE2D.utils.RenderingManager;
import Mirrorz.Main;

public class UIButton extends MouseObject
{
	private boolean clicked = false;
	private boolean prevState;
	
	private String text;
	
	private Rectangle bounds = new Rectangle();
	private UI uiClass = null;
	private UIFont font = new UIFont();
	
	public UIButton(String text, int x, int y, int boundX, int boundY, UI uiClass)
	{
		super(new Rectangle(x, y, boundX, boundY));
		
		this.text = text;
		bounds.x = x;
		bounds.y = y;
		bounds.width = boundX;
		bounds.height = boundY;
		this.uiClass = uiClass;
		
		font.initFont("Arial", Font.BOLD, 20, true);
	}
	
	public boolean buttonClicked()
	{
		return clicked;
	}

	public void update(float mouseX, float mouseY)
	{
		if(bounds.contains(mouseX, (Main.instance.getHeight() - mouseY)))
		{
			if(Mouse.isButtonDown(0) && !MouseUtils.mouseHasObjectGrabbed(this))
			{
				clicked = true;
				
				MouseUtils.setGrabbedObject(this);
		    }
			else
			{
				clicked = false;
			}
			
			if(Mouse.isButtonDown(0) && !prevState && !MouseUtils.mouseHasObjectGrabbed(this))
			{
				execute();
				
		        clicked = true;
		        
		        MouseUtils.setGrabbedObject(this);
		    }
			else
			{
				MouseUtils.setUngrabbed();
			}
			
		    prevState = Mouse.isButtonDown(0);
		}
		else
		{
			clicked = false;
		}
	}
	
	private void execute()
	{
		
	}
	
	public void render()
	{
		if(clicked)
		{
			RenderingManager.drawRoundedBox(bounds.x, bounds.y, bounds.width, bounds.height, new Vector4f(0, 1, 0, 1), new Vector4f(0, 1, 0, 1), false);
			RenderingManager.drawRectangle(bounds.x + bounds.width - 50, bounds.y + 25, 50, 25, new Color(0, 1, 0, false), false);
			RenderingManager.drawRectangle(bounds.x,bounds.y, 50, bounds.height, new Color(0, 1, 0, false), false);
		}
		else if(!clicked)
		{
			RenderingManager.drawRoundedBox(bounds.x, bounds.y, bounds.width, bounds.height, new Vector4f(1, 0, 0, 1), new Vector4f(1, 0, 0, 1), false);
			RenderingManager.drawRectangle(bounds.x + bounds.width - 50, bounds.y + 25, 50, 25, new Color(1, 0, 0, false), false);
			RenderingManager.drawRectangle(bounds.x, bounds.y, 50, bounds.height, new Color(1, 0, 0, false), false);
		}
        
        font.renderUpdateText(bounds.x + (bounds.width - font.ttf.getWidth(text)) / 2, bounds.y + (bounds.height - font.ttf.getHeight(text)) / 2, text, new Color(1, 1, 1, false));
	}
}
