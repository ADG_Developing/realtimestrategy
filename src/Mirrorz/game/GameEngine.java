package Mirrorz.game;

import java.io.File;
import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import AE2D.utils.Color;
import AE2D.utils.RenderingManager;
import Mirrorz.Main;
import Mirrorz.game.engine.LightingEngine;
import Mirrorz.game.engine.StrategyEngine;
import Mirrorz.game.engine.mechanics.CommandHandler;
import Mirrorz.ui.UI;

public class GameEngine 
{
	// Class Variables
	private StrategyEngine strategyEngine = new StrategyEngine();
	private CommandHandler commandHandler = new CommandHandler();
	private LightingEngine lightingEngine = new LightingEngine();
	private Texture backgroundTexture;
	public UI ui = new UI();
	public static GameEngine instance;
	
	// Definition
	public GameEngine()
	{
		commandHandler.registerCommands();
		instance = this;
	}
	
	// Actions
	public void init()
	{
		strategyEngine.init();
		lightingEngine.init();
		ui.initUI();
		
		try {
			backgroundTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "grass.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		lightingEngine.addLightObject(0, 300, 400, new Color(0, 10, 0, false));
		//lightingEngine.addShadowObject(0, 200, 100, 100, 100);
	}
	
	public void renderUI()
	{
		ui.render();
		ui.update(Mouse.getX(), Mouse.getY());
	}
	
	public void render()
	{
		RenderingManager.drawTexturedRectangle(0, 0, Main.instance.getWidth(), Main.instance.getHeight(), backgroundTexture);
		
		strategyEngine.render();	
		lightingEngine.render();
	}
	
	public void update()
	{
		strategyEngine.update();
	    lightingEngine.update();
	}
	
	// Getters & Setters
	public CommandHandler getCommandHandler()
	{
		return commandHandler;
	}
}
