package Mirrorz.game.engine.api;

public class IBuilding 
{
	// Building Variables
	public String name;
	public int lvl;
	
	// Enums
	public enum States
	{
		OPEN(0), CLOSED(1), CONSTRUCTION(2);
		
		public final int value;

	    private States(int value) {
	        this.value = value;
	    }
	};
	
	public enum Types
	{
		HB(0), BARRACKS(1);
		
		public final int value;

	    private Types(int value) {
	        this.value = value;
	    }
	};
	
	// Actions
	public void init() {}
	public void update(boolean opened, boolean construction) {}
	public void render() {}	
	
	// Getters & Setters
	public States getState() {return States.CLOSED;}
	public void setState(int state) {}
}
