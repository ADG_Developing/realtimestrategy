package Mirrorz.game.engine.api;

public class ITroop 
{
	public enum States
	{
		INWAR(0), INVILLAGE(1), HURT(2), IDLE(3);
		
		public final int value;

	    private States(int value) {
	        this.value = value;
	    }
	};
	
	// Actions
	public void update(boolean war, boolean hurt, boolean idle) {}
	public void render() {}	
	
	// Getters & Setters
	public States getState() { return States.IDLE; }
	public void setState(int state) {}
}
