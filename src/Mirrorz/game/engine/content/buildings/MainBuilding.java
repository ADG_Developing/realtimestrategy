package Mirrorz.game.engine.content.buildings;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import AE2D.utils.MouseObject;
import AE2D.utils.MouseUtils;
import AE2D.utils.RenderingManager;
import Mirrorz.Main;
import Mirrorz.game.GameEngine;
import Mirrorz.game.engine.api.IBuilding;
import Mirrorz.ui.UI;
import Mirrorz.ui.UIMenu;

public class MainBuilding extends IBuilding
{
	// Class Variables
	public Rectangle bounds        = new Rectangle();
	public MouseObject mouseObj    = new MouseObject(bounds);
	private List<Texture> textures = new ArrayList<Texture>();
	private UI uiClass             = null;
	private UIMenu menuClass       = null;
	
	// Building Variables 
	public String name             = "Main Building";
	public int lvl                 = 1;
	
	// General Variables
	private boolean menuActivated  = false;
	private boolean clicked        = false;
	private boolean prevState;
	
	// Definition
	public MainBuilding(int x, int y, int width, int height)
	{
		bounds.x = x;
		bounds.y = y;
		bounds.width = width;
		bounds.height = height;
	}
	
	// Actions
	@Override 
	public void init()
	{
		try {
			textures.add(0, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "buildings" + File.separator + "MB.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		uiClass = GameEngine.instance.ui;
		menuClass = new UIMenu(Main.instance.getWidth() / 2 - 500, Main.instance.getHeight() / 2 - 300, 1000, 600, uiClass);
		menuClass.init();
	}
	
	@Override
	public void update(boolean opened, boolean construction)
	{
		if(bounds.contains(Mouse.getX(), (Main.instance.getHeight() - Mouse.getY())))
		{
			if(Mouse.isButtonDown(0) && !MouseUtils.mouseHasObjectGrabbed(mouseObj))
			{
				clicked = true;
				
				MouseUtils.setGrabbedObject(mouseObj);
		    }
			else
			{
				clicked = false;
			}
			
			if(Mouse.isButtonDown(0) && !prevState && !MouseUtils.mouseHasObjectGrabbed(mouseObj))
			{
				if(!menuActivated)
				{
					menuActivated = !menuActivated;
				}
				
		        clicked = true;
		        
		        MouseUtils.setGrabbedObject(mouseObj);
		    }
			else
			{
				MouseUtils.setUngrabbed();
			}
			
		    prevState = Mouse.isButtonDown(0);
		}
		else if(!menuClass.bounds.contains(Mouse.getX(), (Main.instance.getHeight() - Mouse.getY())))
		{
			if(menuActivated)
			{
				if(Mouse.isButtonDown(0) || Mouse.isButtonDown(1) && !prevState && !MouseUtils.mouseHasObjectGrabbed(mouseObj))
				{
					menuActivated = !menuActivated;
			        
			        MouseUtils.setGrabbedObject(mouseObj);
			    }
				else
				{
					MouseUtils.setUngrabbed();
				}
				
				if(Mouse.isButtonDown(0))
				{
					prevState = Mouse.isButtonDown(0);
				}
				else
				{
					prevState = Mouse.isButtonDown(1);
				}
			}
		}
	}
	
	@Override
	public void render() 
	{
		RenderingManager.drawTexturedRectangle(bounds.x, bounds.y, bounds.width, bounds.height, textures.get(0));

		if(menuActivated)
		{
			menuClass.render(name, textures.get(0));
		}
	}	
		
	// Getters & Setters
	@Override
	public States getState() 
	{
		return States.OPEN;
	}
	
	@Override
	public void setState(int state) 
	{
		
	}
}
