package Mirrorz.game.engine.content.commands;

import org.lwjgl.opengl.Display;

import Mirrorz.game.engine.api.ICommand;

public class CommandQuit extends ICommand 
{
	public CommandQuit()
	{
		this.commandName = "quit";
	}
	
	@Override
	public void executeCommand(String arg1, String arg2, String arg3)
	{
		if(arg1.equals("message"))
		{
			if(arg2.equals("disp"))
			{
				if(!arg3.equals(""))
				{
					System.out.println(arg3);
					
					Display.destroy();
					System.exit(0);
				}
			}
		}
		else
		{
			Display.destroy();
			System.exit(0);
		}
	}
}
