package Mirrorz.game.engine.content.commands;

import Mirrorz.game.engine.StrategyEngine;
import Mirrorz.game.engine.api.ICommand;

public class CommandResource extends ICommand 
{
	public CommandResource()
	{
		this.commandName = "resource";
	}
	
	@Override
	public void executeCommand(String arg1, String arg2, String arg3)
	{
		if(arg1.equals("add"))
		{
			if(!arg2.equals(""))
			{
				if(!arg3.equals(""))
				{
					if(Integer.parseInt(arg3) >= 0)
					{
						StrategyEngine.instance.addResource(Integer.parseInt(arg2), Integer.parseInt(arg3));
					}
				}
			}
		}
		
		if(arg1.equals("set"))
		{
			if(!arg2.equals(""))
			{
				if(!arg3.equals(""))
				{
					if(Integer.parseInt(arg3) >= 0)
					{
						StrategyEngine.instance.setResource(Integer.parseInt(arg2), Integer.parseInt(arg3));
					}
				}
			}
		}
		
		if(arg1.equals("take"))
		{
			if(!arg2.equals(""))
			{
				if(!arg3.equals(""))
				{
					if(StrategyEngine.instance.getResource(Integer.parseInt(arg2)) - Integer.parseInt(arg3) >= 0)
					{
						StrategyEngine.instance.setResource(Integer.parseInt(arg2), StrategyEngine.instance.getResource(Integer.parseInt(arg2)) - Integer.parseInt(arg3));
					}
					else
					{
						
					}
				}
			}
		}
	}
}
