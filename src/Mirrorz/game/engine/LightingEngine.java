package Mirrorz.game.engine;

import static org.lwjgl.opengl.GL11.GL_ALWAYS;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_EQUAL;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_KEEP;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_REPLACE;
import static org.lwjgl.opengl.GL11.GL_STENCIL_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glColorMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glStencilFunc;
import static org.lwjgl.opengl.GL11.glStencilOp;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector2f;

import AE2D.utils.Color;
import AE2D.utils.LightObject;
import AE2D.utils.ShadowObject;
import Mirrorz.Main;

public class LightingEngine 
{
	// Class Variables
	private ArrayList<LightObject> lightObjects = new ArrayList<LightObject>();
	private ArrayList<ShadowObject> shadowObjects = new ArrayList<ShadowObject>();
	private BufferedReader reader;
	
	// Engine Variables
	private int fragmentShader;
	private int shaderProgram;
	private boolean reverseClock;
	
	// Definition
	public LightingEngine()
	{
		
	}
	
	public void fadeLightColor(Color newColor, Color oldColor, int ID, float t) 
	{
        for(int i = 30; i > 0; i--)
		{
			double ratio = 30.f / (i / t);

		    float red = (int)Math.abs((ratio * newColor.getR()) + ((1 - ratio) * oldColor.getR()));
		    float green = (int)Math.abs((ratio * newColor.getG()) + ((1 - ratio) * oldColor.getG()));
		    float blue = (int)Math.abs((ratio * newColor.getB()) + ((1 - ratio) * oldColor.getB()));
			
			if(red == newColor.getR() && green == newColor.getG() && blue == newColor.getB())
			{
				reverseClock = true;
			}
			
	        lightObjects.get(ID).setColor(new Color(red * t, green * t, blue * t, true));
	    }
	}
	
	// Actions
	public void init()
	{
		shaderProgram = glCreateProgram();
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		StringBuilder fragmentShaderSource = new StringBuilder();

		try {
			String line;
			reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + File.separator + "data" + File.separator + "shaders" + File.separator + "shader.frag"));
			while ((line = reader.readLine()) != null) {
				fragmentShaderSource.append(line).append("\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		glShaderSource(fragmentShader, fragmentShaderSource);
		glCompileShader(fragmentShader);
		if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == GL_FALSE) {
			System.err.println(GL20.glGetShaderInfoLog(fragmentShader, 10000000));
		}

		glAttachShader(shaderProgram, fragmentShader);
		glLinkProgram(shaderProgram);
		glValidateProgram(shaderProgram);
	}
	
	public void render()
	{
		for (LightObject lightObj : lightObjects) {
			glColorMask(false, false, false, false);
			glStencilFunc(GL_ALWAYS, 1, 1);
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

			for (ShadowObject shadowObj : shadowObjects) {
				Vector2f[] vertices = shadowObj.getVertices();
				for (int i = 0; i < vertices.length; i++) {
					Vector2f currentVertex = vertices[i];
					Vector2f nextVertex = vertices[(i + 1) % vertices.length];
					Vector2f edge = Vector2f.sub(nextVertex, currentVertex, null);
					Vector2f normal = new Vector2f(edge.getY(), -edge.getX());
					Vector2f lightToCurrent = Vector2f.sub(currentVertex, lightObj.getLocation(), null);
					if (Vector2f.dot(normal, lightToCurrent) > 0) {
						Vector2f point1 = Vector2f.add(currentVertex, (Vector2f) Vector2f.sub(currentVertex, lightObj.getLocation(), null).scale(Main.instance.getWidth()), null);
						Vector2f point2 = Vector2f.add(nextVertex, (Vector2f) Vector2f.sub(nextVertex, lightObj.getLocation(), null).scale(Main.instance.getWidth()), null);
						
						glBegin(GL_QUADS); {
							glVertex2f(currentVertex.getX(), currentVertex.getY());
							glVertex2f(point1.getX(), point1.getY());
							glVertex2f(point2.getX(), point2.getY());
							glVertex2f(nextVertex.getX(), nextVertex.getY());
						} glEnd();
					}
				}
			}
			
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
			glStencilFunc(GL_EQUAL, 0, 1);
			glColorMask(true, true, true, true);

			glUseProgram(shaderProgram);
			glUniform2f(glGetUniformLocation(shaderProgram, "lightLocation"), lightObj.getLocation().getX(), Main.instance.getHeight() - lightObj.getLocation().getY());
			glUniform3f(glGetUniformLocation(shaderProgram, "lightColor"), lightObj.getColor().getR(), lightObj.getColor().getG(), lightObj.getColor().getB());
			GL20.glUniform1f(glGetUniformLocation(shaderProgram, "radius"), 10);
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);

			glBegin(GL_QUADS); {
				glVertex2f(0, 0);
				glVertex2f(0, Main.instance.getHeight());
				glVertex2f(Main.instance.getWidth(), Main.instance.getHeight());
				glVertex2f(Main.instance.getWidth(), 0);
			} glEnd();

			glDisable(GL_BLEND);
			glUseProgram(0);
			glClear(GL_STENCIL_BUFFER_BIT);
		}
		
		glEnable(GL_BLEND);
		glColor3f(0, 0, 0);
		for (ShadowObject shadowObj : shadowObjects) {
			glBegin(GL11.GL_QUADS); {
				for (Vector2f vertex : shadowObj.getVertices()) {
					glVertex2f(vertex.getX(), vertex.getY());
				}
			} glEnd();
		}
		glDisable(GL_BLEND);
	}
	
	public void update()
	{
		
	}
	
	// Getters, Setters, Adders & Removers	
	public ShadowObject getShadowObject(int ID)
	{
		return shadowObjects.get(ID);
	}
	
	public void setShadowObject(int ID, int x, int y, int width, int height)
	{
		shadowObjects.set(ID, new ShadowObject(x, y, width, height));
	}
	
	public void addShadowObject(int ID, int x, int y, int width, int height)
	{
		shadowObjects.add(ID, new ShadowObject(x, y, width, height));
	}
	
	public void removeShadowObject(int ID)
	{
		shadowObjects.remove(ID);
	}
	
	public LightObject getLightObject(int ID)
	{
		return lightObjects.get(ID);
	}
	
	public void setLightObject(int ID, int x, int y, Color color)
	{
		lightObjects.set(ID, new LightObject(x, y, color));
	}
	
	public void addLightObject(int ID, int x, int y, Color color)
	{
		lightObjects.add(ID, new LightObject(x, y, color));
	}
	
	public void removeLightObject(int ID)
	{
		lightObjects.remove(ID);
	}
}
