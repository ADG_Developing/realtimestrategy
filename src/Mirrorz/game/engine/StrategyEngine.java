package Mirrorz.game.engine;

import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import AE2D.ui.UIFont;
import AE2D.utils.RenderingManager;
import Mirrorz.game.engine.mechanics.BuildingHandler;
import Mirrorz.game.engine.mechanics.TroopHandler;

public class StrategyEngine 
{
	// Class Variables
	private BuildingHandler buildingHandler = new BuildingHandler();
	private TroopHandler troopHandler = new TroopHandler();
	private UIFont font = new UIFont();
	private List<Texture> textures;
	public static StrategyEngine instance;
	
	// Player variables
	private String playerName = "";
	
	// Village variables
	private List<Integer> playerResources;
	
	// Definition
	public StrategyEngine()
	{
		textures        = new ArrayList<Texture>();
		playerResources = new ArrayList<Integer>();
		
		playerResources.add(0, 200);  // Gold
		playerResources.add(1, 200);  // Silver
		playerResources.add(2, 400);  // Coal
		playerResources.add(3, 400);  // Iron
		playerResources.add(4, 400);  // Wood
		playerResources.add(5, 400);  // Clay
		playerResources.add(6, 400);  // Crop
		playerResources.add(7, 1000); // Water
		
		instance = this;
	}
	
	// Actions
	public void init()
	{
		font.initFont("Arial", Font.BOLD, 18, true);
		
		try {
			textures.add(0, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "gold.png")));
			textures.add(1, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(System.getProperty("user.dir") + File.separator + "data" + File.separator + "textures" + File.separator + "wood.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		buildingHandler.init();
	}
	
	public void update()
	{
		buildingHandler.update();
		troopHandler.update();
	}
	
	public void render()
	{
		RenderingManager.drawResource(10, 40, textures.get(0), font, playerResources.get(0));
		RenderingManager.drawResource(10, 65, textures.get(1), font, playerResources.get(4));
		
		buildingHandler.render();
		troopHandler.render();
	}
	
	// Resource Actions.
	public void addResource(int resourceType, int num)
	{
		switch(resourceType)
		{
		case 0:
			playerResources.set(0, playerResources.get(0) + num);
			break;
		case 1:
			playerResources.set(1, playerResources.get(1) + num);
			break;
		case 2:
			playerResources.set(2, playerResources.get(2) + num);
			break;
		case 3:
			playerResources.set(3, playerResources.get(3) + num);
			break;
		case 4:
			playerResources.set(4, playerResources.get(4) + num);
			break;
		case 5:
			playerResources.set(5, playerResources.get(5) + num);
			break;
		case 6:
			playerResources.set(6, playerResources.get(6) + num);
			break;
		case 7:
			playerResources.set(7, playerResources.get(7) + num);
			break;
		}
	}
	
	public void setResource(int resourceType, int num)
	{
		switch(resourceType)
		{
		case 0:
			playerResources.set(0, num);
			break;
		case 1:
			playerResources.set(1, num);
			break;
		case 2:
			playerResources.set(2, num);
			break;
		case 3:
			playerResources.set(3, num);
			break;
		case 4:
			playerResources.set(4, num);
			break;
		case 5:
			playerResources.set(5, num);
			break;
		case 6:
			playerResources.set(6, num);
			break;
		case 7:
			playerResources.set(7, num);
			break;
		}
	}
	
	public int getResource(int resourceType)
	{
		switch(resourceType)
		{
		case 0:
			return playerResources.get(0);
		case 1:
			return playerResources.get(1);
		case 2:
			return playerResources.get(2);
		case 3:
			return playerResources.get(3);
		case 4:
			return playerResources.get(4);
		case 5:
			return playerResources.get(5);
		case 6:
			return playerResources.get(6);
		case 7:
			return playerResources.get(7);
		}
		
		return 0;
	}
}
