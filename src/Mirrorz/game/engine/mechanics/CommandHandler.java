package Mirrorz.game.engine.mechanics;

import java.util.ArrayList;
import java.util.List;

import Mirrorz.game.engine.api.ICommand;
import Mirrorz.game.engine.content.commands.CommandQuit;
import Mirrorz.game.engine.content.commands.CommandResource;

public class CommandHandler 
{
	private List<ICommand> commands;
	private List<String> commandNames;
	
	public void registerCommands()
	{
		commands = new ArrayList<ICommand>();
		commandNames = new ArrayList<String>();
		
		commands.add(new CommandQuit());
		commandNames.add(commands.get(0).commandName);
		
		commands.add(new CommandResource());
		commandNames.add(commands.get(1).commandName);
	}
	
	public boolean processInput(String input)
	{
		int counter = 0;
		
		for(ICommand cmd : commands)
		{
			if(cmd != null)
			{	
				if(input.substring(1).split(" ")[0].toString().equals(commandNames.get(counter).toString()))
				{		
					if(input.substring(1).split(" ").length == 1)
					{
						cmd.executeCommand("", "", "");
					}
					else if(input.substring(1).split(" ").length == 2)
					{
						cmd.executeCommand(input.substring(1).split(" ")[1], "", "");
					}
					else if(input.substring(1).split(" ").length == 3)
					{
						cmd.executeCommand(input.substring(1).split(" ")[1], input.substring(1).split(" ")[2], "");
					}		
					else if(input.substring(1).split(" ").length == 4)
					{
						//System.out.println(input.substring(1).split(" ")[2]);
						cmd.executeCommand(input.substring(1).split(" ")[1], input.substring(1).split(" ")[2], input.substring(1).split(" ")[3]);
					}
					
					return true;
				}
			}
			
			counter++;
		}
		
		return false;
	}
}
