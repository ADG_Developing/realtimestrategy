package Mirrorz.game.engine.mechanics;

import java.util.ArrayList;
import java.util.List;

import Mirrorz.game.engine.api.IBuilding;
import Mirrorz.game.engine.api.IBuilding.States;
import Mirrorz.game.engine.content.buildings.MainBuilding;

public class BuildingHandler 
{
	// Class Variables
	private List<IBuilding> buildings;
	
	// Definition
	public BuildingHandler()
	{
		buildings = new ArrayList<IBuilding>();
		buildings.add(new MainBuilding(650, 400, 100, 100));
	}
	
	// Actions
	public void init()
	{
		for(IBuilding tmp : this.buildings)
		{
			if(tmp != null)
			{
				tmp.init();
			}
		}
	}
	
	public void update()
	{
		for(IBuilding tmp : this.buildings)
		{
			if(tmp != null)
			{
				if(tmp.getState().value == States.OPEN.value) { tmp.update(true, false); }
				else
				if(tmp.getState().value == States.CLOSED.value) { tmp.update(false, false); }
				else
				if(tmp.getState().value == States.CONSTRUCTION.value) { tmp.update(true, true); }
			}
		}
	}
	
	public void render()
	{
		for(IBuilding tmp : this.buildings)
		{
			if(tmp != null)
			{
				tmp.render();
			}
		}
	}
}
