package Mirrorz.game.engine.mechanics;

import java.util.ArrayList;
import java.util.List;

import Mirrorz.game.engine.api.ITroop;
import Mirrorz.game.engine.api.ITroop.States;

public class TroopHandler 
{
	// Class Variables
	private List<ITroop> troops;
	
	// Definition
	public TroopHandler()
	{
		troops = new ArrayList<ITroop>();
	}
	
	// Actions
	public void update()
	{
		for(ITroop tmp : troops)
		{
			if(tmp != null)
			{
				if(tmp.getState().value == States.INVILLAGE.value) { tmp.update(false, false, false); }
				else
				if(tmp.getState().value == States.INWAR.value) { tmp.update(true, false, false); }
				else
				if(tmp.getState().value == States.HURT.value) { tmp.update(false, true, false); }
				else
				if(tmp.getState().value == States.IDLE.value) { tmp.update(false, false, true); }
			}
		}
	}
	
	public void render()
	{
		for(ITroop tmp : troops)
		{
			if(tmp != null)
			{
				tmp.render();
			}
		}
	}
}
