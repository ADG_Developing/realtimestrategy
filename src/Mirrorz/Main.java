package Mirrorz;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_STENCIL_TEST;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glValidateProgram;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import Mirrorz.game.GameEngine;
 
public class Main 
{
	// Class Variables
	private GameEngine gameEngine = new GameEngine();
	private BufferedReader reader;
	public static Main instance;
	
	// General Variables
	private int fragmentShader;
	private int shaderProgram;
	private final int width = 1400;
	private final int height = 800;

	
	public void startGame()
	{
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.create();
		} catch (LWJGLException e) {
			System.exit(0);
			e.printStackTrace();
		}
		
		// Init OpenGL here.
		initGL();
		
		// General Init
		gameEngine.init();
		
		// Game loop. Used for rendering and updating.
		while(!Display.isCloseRequested())
		{
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			GL11.glEnable(GL11.GL_BLEND);
			gameEngine.render();
			GL11.glDisable(GL11.GL_BLEND);
			
			gameEngine.renderUI();
			gameEngine.update();
			
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			{
				System.exit(0);
				Display.destroy();
			}
			
			Display.update();
		}
		
		Display.destroy();
	}

	public void initGL()
	{		
		GL11.glClearColor(0, (1.f / 255.f) * 123.f, (1.f / 255.f) * 12.f, 1);
		
		shaderProgram = glCreateProgram();
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		StringBuilder fragmentShaderSource = new StringBuilder();

		try {
			String line;
			reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + File.separator + "data" + File.separator + "shaders" + File.separator + "shader.frag"));
			while ((line = reader.readLine()) != null) {
				fragmentShaderSource.append(line).append("\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		glShaderSource(fragmentShader, fragmentShaderSource);
		glCompileShader(fragmentShader);
		if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == GL_FALSE) {
			System.err.println(GL20.glGetShaderInfoLog(fragmentShader, 10000000));
		}

		glAttachShader(shaderProgram, fragmentShader);
		glLinkProgram(shaderProgram);
		glValidateProgram(shaderProgram);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);

		glEnable(GL_STENCIL_TEST);
	}
	
	public static void main(String[] args)
	{
        String OS = System.getProperty("os.name");
        if (OS.toLowerCase().contains("windows")) 
        	OS = "windows";
        else if (OS.toLowerCase().contains("mac")) 
        	OS = "macosx";
        else if (OS.toLowerCase().contains("linux")) 
        	OS = "linux";
        else 
        	OS = "Error";
        
        if (OS == "Error")
        {
        	System.exit(1);
        }
        
		System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + File.separator +"lib" + File.separator + "lwjgl"+ File.separator +"native"+ File.separator + OS);    
		
		instance = new Main();
		instance.startGame();
	}
	
	// Getters & Setters
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
}